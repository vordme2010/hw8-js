//обработчик событий в js - это часть кода - действие, которое будет срабатывать при запуске события
let input = document.querySelector("#input")

input.addEventListener("focusin", (event) => {
    event.target.style.border = "5px solid green"
});

input.addEventListener("focusout", event => {
    event.target.style.border = "";
    incorrectPrice = document.querySelector(".incorrectPriceContainer")
    if (event.target.value < 0) {
        getElementValueLessThanZero(
            "color: red; border: 5px solid red;",
            "margin-top: 10px; border: 3px solid red; border-radius: 40px; padding: 6px; text-align: center; width: 200px;",
            "color: red;"
        )
    } 
    else if (event.target.value > 0) {
        getElementValueMoreThanZero(
            "color: green; border: 5px solid green;", 
            "margin-top: 10px; border: 3px solid grey; border-radius: 40px; padding: 6px; width: 200px;",
            "border: 1px solid black; border-radius: 20%; padding: 4px; margin: 0 10px 0 5px; cursor: pointer;"
        )
    }
});

function getElementValueLessThanZero(inputStyles, divStyles, textStyles) {
    event.target.style.cssText = inputStyles
    if (!incorrectPrice) {
    const incorrectPriceContainer = createAndAppendElement("div", "incorrectPriceContainer", divStyles, "", document.body)
    createAndAppendElement("span", "", textStyles, "invalid price!", incorrectPriceContainer)
    }
}

function getElementValueMoreThanZero(inputStyles, divStyles, exitSpanStyles) {
    if (incorrectPrice){
        incorrectPrice.remove()
    }
    event.target.style.cssText = inputStyles
    const correctPriceContainer = createAndAppendElement("div", "correctPriceContainer", divStyles, "", document.body)
    const exitSpan = createAndAppendElement("span", "", exitSpanStyles, "X", correctPriceContainer)
    createAndAppendElement("span", "", "", `Current price: ${event.target.value}$`, correctPriceContainer)
    exitSpan.onclick = function () {
        event.target.value = ""
        event.target.style = ""
        correctPriceContainer.remove();
    }
}

function createAndAppendElement(tagname, className = [], attributes = {}, textContent, parentElement) {
    let element = document.createElement(tagname)
    element.innerHTML = textContent
    element.className = className
    element.style.cssText = attributes
    return parentElement.appendChild(element)
}